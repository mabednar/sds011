# -*- coding: utf-8 -*-
# @Author: marek
# @Date:   2018-02-10 22:35:47
# @Last Modified by:   marek
# @Last Modified time: 2018-02-10 23:22:26

import serial, time, struct, array
from datetime import datetime

# WHO max norm per 24h
pm_2_who = 25
pm_10_who = 50


def main():
    s = serial.Serial()
    s.port = "/dev/ttyUSB0"
    s.baudrate = 9600

    s.open()
    if not s.is_open:
        print(f"serial not open: {s.port}")
        raise ValueError

    s.flushInput()
    byte, lastbyte = "\x00", "\x00"
    try:
        while True:
            lastbyte = byte
            byte = s.read(size=1)
            if lastbyte == b"\xaa" and byte == b"\xc0":
                data = s.read(size=8)
                mesurement = struct.unpack('<hhxxcc',data)
                pm_2  = mesurement[0]/10.0
                pm_10 = mesurement[1]/10.0
                info = f"PM 2.5={pm_2:.1f} μg/m^3 {pm_2/pm_2_who*100:.1f}%, PM 10={pm_10:.1f} μg/m^3 {pm_10/pm_10_who*100:.1f}%"
                date = datetime.now().strftime("%d %b %Y %H:%M:%S: ")
                print(f"{date}{info}")
    except:
        pass
    finally:
        s.close()


if __name__ == '__main__':
    main()
