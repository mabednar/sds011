import pytest
from sds011 import SDS011


class TestContextManager:

    def test_raise_channel(self):
        with pytest.raises(ValueError):
            with SDS011(port=None) as sensor:
                pass


class TestMessage:

    def test_set_data_report_mode(self):
        
