msg = {
    "set_data_report_mode": {
        "host": "AA B4 02 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 00 AB",
        "resp": (
                    "AA C5 02 00 00 00 A1 60 03 AB",
                    "AA C5 02 00 01 00 A1 60 04 AB"
                )

    }
}



def convert(hex_string):
    hex_list = [h.lower() for h in hex_string]
    hex_list = [bytearray.fromhex(h) for h in hex_list]
    return hex_list


