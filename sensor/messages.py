from collections import OrderedDict

class BaseMessage(OrderedDict):

    def __init__(self,
                 head="aa",
                 command_id="b4",
                 data_1="00",
                 data_2="00",
                 data_3="00",
                 data_4="00",
                 data_5="00",
                 data_6="00",
                 data_7="00",
                 data_8="00",
                 data_9="00",
                 data_10="00",
                 data_11="00",
                 data_12="00",
                 data_13="00",
                 data_14="ff",
                 data_15="ff",
                 checksum=None,
                 tail="ab"
                ):
        self["head"] = head
        self["command_id"] = command_id
        self["data_1"] = data_1
        self["data_2"] = data_2
        self["data_3"] = data_3
        self["data_4"] = data_4
        self["data_5"] = data_5
        self["data_6"] = data_6
        self["data_7"] = data_7
        self["data_8"] = data_8
        self["data_9"] = data_9
        self["data_10"] = data_10
        self["data_11"] = data_11
        self["data_12"] = data_12
        self["data_13"] = data_13
        self["data_14"] = data_14
        self["data_15"] = data_15
        self["checksum"] = self._calc_checksum() if checksum == None else checksum
        self["tail"] = tail

    def _calc_checksum(self):
        s = 0
        for data in list(self.values())[2:]:
            s += int(data, 16)
        checksum = f"{s:x}"[-2:]
        checksum = checksum if len(checksum) == 2 else "".join(("0",checksum))
        return checksum

    def _set_reciever(self, device_id):
        return {"data_14":device_id[:2], "data_15":device_id[2:]}

    @property
    def binary(self):
        data = [h for h in self.values()]
        return(data)


class BaseResponse(BaseMessage):

    def __init__(self,
                 head="aa",
                 command_id="b4",
                 data_1="00",
                 data_2="00",
                 data_3="00",
                 data_4="00",
                 data_5="00",
                 data_6="00",
                 checksum=None,
                 tail="ab"
                ):
        self["head"] = head
        self["command_id"] = command_id
        self["data_1"] = data_1
        self["data_2"] = data_2
        self["data_3"] = data_3
        self["data_4"] = data_4
        self["data_5"] = data_5
        self["data_6"] = data_6
        self["checksum"] = self._calc_checksum() if checksum == None else checksum
        self["tail"] = tail

    def get_sender(self):
        return self.data_5 + self.data_6


class SetDataRespondingModeMsg(BaseMessage):

    def __init__(self, mode, reciever="all"):
        super().__init__(data_1="02",
                         data_2="01",
                         **self._set_mode(mode),
                         **self._set_reciever(reciever))

    def _set_mode(self, mode):
        return {"data_3":"01"} if mode == "query" else {"data_3":"00"}


class SetDeviceIDMsg(BaseMessage):

    def __init__(self, new_device_id, reciever="all"):
        super().__init__(data_1="05",
                         **self._set_new_device_id(new_device_id),
                         **self._set_reciever(reciever))

    def _set_new_device_id(self, new_device_id):
        return {"data_12":new_device_id[:2], "data_13":new_device_id[2:]}


class SetSleepWorkMsg(BaseMessage):

    def __init__(self, mode, reciever="all"):
        super().__init__(data_1="06",
                         data_2="01",
                         **self._set_mode(mode),
                         **self._set_reciever(reciever))

    def _set_mode(self, mode):
        return {"data_3":"01"} if mode=="work" else {"data_3":"00"}


class SetWorkingPeriodMsg(BaseMessage):

    def __init__(self, interval=0, reciever="all"):
        super().__init__(data_1="08",
                         data_2="01",
                         **self._set_interval(interval),
                         **self._set_reciever(reciever))

    def _set_interval(self, interval):
        return {"data_3":interval} if interval > 30 else {"data_3":30}


class QueryDataRespondingModeMsg(BaseMessage):

    def __init__(self, mode, reciever="all"):
        super().__init__(data_1="02", **self._set_reciever(reciever))


class QuerySleepWorkMsg(BaseMessage):

    def __init__(self, reciever="all"):
        super().__init__(data_1="06", **self._set_reciever(reciever))


class QueryFirmwareVersionMsg(BaseMessage):

    def __init__(self, reciever="all"):
            super().__init__(data_1="07", **self._set_reciever(reciever))


class QueryDataMsg(BaseMessage):

    def __init__(self, reciever="all"):
            super().__init__(data_1="04", **self._set_reciever(reciever))



bm = BaseMessage(data_1="04")
br = BaseResponse()
# print(br.binary)
# print(br.data_14)

# sd = SetDeviceIDMsg("1234", "2233")
# print(f"sd = {sd.binary}")
# print(sd.keys())

s = SetSleepWorkMsg(mode="work", reciever="a160")
print(s.binary)

# sensor = SDS1()
# sensor.query_mode()
# sensor.set_mode(mode=)
